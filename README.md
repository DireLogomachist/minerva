# Minerva #
A voice command automation system for Windows

Minerva is easily extendable with custom scripts and features to do things like volume control, running Steam games by name, performing searches, and opening up your daily batch of webcomics  
Written in C# with use of the .NET System.Speech framework