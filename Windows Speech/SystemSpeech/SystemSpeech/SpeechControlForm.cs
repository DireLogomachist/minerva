﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Speech;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.IO;
using System.Diagnostics;
using System.Runtime.Remoting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SystemSpeech {
    public partial class SpeechControlForm : Form {
        SpeechRecognitionEngine sre;
        SpeechSynthesizer ss;

        Grammar defaultG;
        Grammar partialG;
        Grammar postfixG;

        Grammar commandGrammar;
        Grammar commandPreGrammar;
        Grammar commandParGrammar;
        Grammar commandPreParGrammar;

        Grammar familiarCommandGrammar;
        Grammar familiarCommandPreGrammar;
        Grammar familiarCommandParGrammar;
        Grammar familiarCommandPreParGrammar;
        
        Dictionary<string, Dictionary<string, string>> commandTable;

        string cmdListFile = @"..\..\..\..\..\Command Scripts\config\CmdList.json";
        string inputGreetingFile = @"..\..\..\..\..\Command Scripts\config\InputGreeting.json";
        string inputNameFile = @"..\..\..\..\..\Command Scripts\config\InputName.json";
        string inputPolitePrefixFile = @"..\..\..\..\..\Command Scripts\config\InputPolitePrefix.json";
        string inputCmdPrefixFile = @"..\..\..\..\..\Command Scripts\config\InputCmdPrefix.json";
        string inputArticleFile = @"..\..\..\..\..\Command Scripts\config\InputArticle.json";
        string inputPolitePostfixFile = @"..\..\..\..\..\Command Scripts\config\InputPolitePostfix.json";

        string[] greetingStr;
        string[] politenessPrefixStr;
        string[] nameStr;
        string[] cmdPrefixStr;
        string[] articleStr;
        string[] politenessPostfixStr;

        string[] commandStr;
        string[] commandStrWithPrefix;
        string[] commandStrWithParam;
        string[] commandStrWithPrefixParam;

        string _ZiraPro = "Microsoft Server Speech Text to Speech Voice (en-US, ZiraPro)";  //High quality
        string _Hazel = "Microsoft Server Speech Text to Speech Voice (en-GB, Hazel)";      //High quality
        string _Helen = "Microsoft Server Speech Text to Speech Voice (en-US, Helen)";      //Mid quality
        string _Heather = "Microsoft Server Speech Text to Speech Voice (en-CA, Heather)";  //Terrible quality
        string _Hayley = "Microsoft Server Speech Text to Speech Voice (en-AU, Hayley)";    //Terrible quality
        string _Heera = "Microsoft Server Speech Text to Speech Voice (en-IN, Heera)";      //Terrible quality

        static readonly string[] settings = new string[] {"ResourceUsage", "ResponseSpeed", "ComplexResponseSpeed","AdaptationOn", "PersistedBackgroundAdaptation", "CFGConfidenceRejectionThreshold", "HighConfidenceThreshold",  "NormalConfidenceThreshold", "LowConfidenceThreshold"};

        public SpeechControlForm() {
            InitializeComponent();
            Rectangle r = Screen.PrimaryScreen.WorkingArea;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Width, Screen.PrimaryScreen.WorkingArea.Height - this.Height);

            ContextMenu iconMenu = new ContextMenu();
            MenuItem aaah = new MenuItem();
            aaah.Text = "AAAAAH";
            //aaah.Click += OnOffSwitch.;

            iconMenu.MenuItems.Add(aaah);

            trayIcon.ContextMenu = iconMenu;


        }

        private void SpeechControlForm_Load(object sender, EventArgs e) {
            //Congigure recognizer and synthesizer
            sre = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("en-US"));
            ss = new SpeechSynthesizer();
            try {
                ss.SelectVoice(_ZiraPro);
            } catch {
                //ZiraPro not installed...
            }
            
            sre.SetInputToDefaultAudioDevice();
            ss.SetOutputToDefaultAudioDevice();

            System.Globalization.CultureInfo usCulture =  new System.Globalization.CultureInfo("en-US");

            commandTable = buildCommandTable(cmdListFile);
            separateCommandStrings();

            //Set up grammars and grammar string arrays
            //greetingStr = stringArrayFromFile(@"..\..\..\..\..\Command Scripts\config\InputGreeting.ini");
            greetingStr = stringArrayFromJsonList(inputGreetingFile);
            Choices greeting = new Choices(greetingStr);
            GrammarBuilder greetingOpt = new GrammarBuilder(greeting, 0, 1);

            //nameStr = stringArrayFromFile(@"..\..\..\..\..\Command Scripts\config\InputName.ini");
            nameStr = stringArrayFromJsonList(inputNameFile);
            Choices name = new Choices(nameStr);
            GrammarBuilder namesOpt = new GrammarBuilder(name, 0, 1); //Only for use in postfix commands

            //politenessPrefixStr = stringArrayFromFile(@"..\..\..\..\..\Command Scripts\config\InputPolitePrefix.ini");
            politenessPrefixStr = stringArrayFromJsonList(inputPolitePrefixFile);
            Choices politenessPrefix = new Choices(politenessPrefixStr);
            GrammarBuilder politenessPrefixOpt = new GrammarBuilder(politenessPrefix, 0, 1);

            //cmdPrefixStr = stringArrayFromFile(@"..\..\..\..\..\Command Scripts\config\InputCmdPrefix.ini");
            cmdPrefixStr = stringArrayFromJsonList(inputCmdPrefixFile);
            Choices cmdPrefix = new Choices(cmdPrefixStr);
            GrammarBuilder cmdPrefixOpt = new GrammarBuilder(cmdPrefix, 0, 1); //Command Prefix optional

            //articleStr = stringArrayFromFile(@"..\..\..\..\..\Command Scripts\config\InputArticle.ini");
            articleStr = stringArrayFromJsonList(inputArticleFile);
            Choices article = new Choices(articleStr);
            GrammarBuilder articleOpt = new GrammarBuilder(article, 0, 1); //Article optional

            //commandStr = stringArrayFromFile(@"..\..\..\..\..\Command Scripts\config\InputCommand.ini");
            commandStr = commandTable.Keys.ToArray();
            Choices command = new Choices(commandStr);
            GrammarBuilder commandOpt = new GrammarBuilder(command, 0, 1); //Command optional

            //politenessPostfixStr = stringArrayFromFile(@"..\..\..\..\..\Command Scripts\config\InputPolitePostfix.ini");
            politenessPostfixStr = stringArrayFromJsonList(inputPolitePostfixFile);
            Choices politenessPostfix = new Choices(politenessPostfixStr);
            GrammarBuilder politenessPostfixOpt = new GrammarBuilder(politenessPostfix, 0, 1); //Postfix optional

            //----------------------------------------------------------------------------------------------------------------------------------------------------|
            //Afer separate command strings, make choices from
            Choices commandPre = new Choices(commandStrWithPrefix);
            Choices commandPar = new Choices(commandStrWithParam);
            Choices commandPrePar = new Choices(commandStrWithPrefixParam);

            //-----------------------------------------------------------------------------------------------------------------------------------------------------|
            
            //Default command Grammar - only accepts full commands
            GrammarBuilder defaultGB = new GrammarBuilder();
            defaultGB.Culture = new System.Globalization.CultureInfo("en-US");
            List<GrammarBuilder> defaultSubs = new List<GrammarBuilder>() {greetingOpt, name, politenessPrefixOpt, cmdPrefix, articleOpt, command};
            /*grammarBuilderAdditions(ref defaultGB, defaultSubs);
            defaultGB.AppendDictation();
            defaultGB.Append(politenessPostfixGB); //Optional*/

            defaultGB.Append(greetingOpt); //Optional
            defaultGB.Append(name);
            defaultGB.Append(politenessPrefixOpt); //Optional
            defaultGB.Append(cmdPrefix);
            defaultGB.Append(articleOpt); //Optional
            defaultGB.Append(command);
            defaultGB.AppendDictation();
            defaultGB.Append(politenessPostfixOpt); //Optional
            
            defaultG = new Grammar(defaultGB);
            defaultG.Name = "Full Command";
            defaultG.Priority = 50; //new
            defaultG.Weight = 1.0f; //new
            //sre.LoadGrammar(defaultG);
            
            //Partial command grammar - will ask for clarification or postfix command
            GrammarBuilder partialGB = new GrammarBuilder();
            partialGB.Culture = new System.Globalization.CultureInfo("en-US");
            List<GrammarBuilder> partialSubs = new List<GrammarBuilder>() {greetingOpt, name, politenessPrefixOpt, cmdPrefixOpt, cmdPrefixOpt, articleOpt, commandOpt, politenessPostfixOpt};
            //grammarBuilderAdditions(ref partialGB, partialSubs);
            partialGB.Append(greetingOpt);
            partialGB.Append(name);   //Only the name is mandatory - all other input optional
            partialGB.Append(politenessPrefixOpt);
            partialGB.Append(cmdPrefixOpt);
            partialGB.Append(articleOpt);
            partialGB.Append(commandOpt);
            //partialGB.AppendDictation(); //Not sure if needed at all, patial commands shouldn't need it
            partialGB.Append(politenessPostfixOpt);

            partialG = new Grammar(partialGB);
            partialG.Name = "Partial Command";
            partialG.Priority = -50; //new
            partialG.Weight = .2f;   //new
            //sre.LoadGrammar(partialG);

            //Postfix command grammar -  same as full command but without greeting or name
            GrammarBuilder postfixGB = new GrammarBuilder();
            postfixGB.Culture = new System.Globalization.CultureInfo("en-US");
            List<GrammarBuilder> postfixSubs = new List<GrammarBuilder>() {politenessPrefixOpt, cmdPrefix, cmdPrefix, articleOpt, command};
            /*grammarBuilderAdditions(ref postfixGB, postfixSubs);
            postfixGB.AppendDictation();
            postfixGB.Append(politenessPostfixGB);*/
            postfixGB.Append(politenessPrefixOpt);
            postfixGB.Append(cmdPrefix);
            postfixGB.Append(articleOpt);
            postfixGB.Append(command);
            postfixGB.AppendDictation();
            postfixGB.Append(politenessPostfixOpt);

            postfixG = new Grammar(postfixGB);
            postfixG.Name = "Postfix Command"; //Only loaded after partial command grammar recognized and then unloaded after timeout
            



            //-----------------------------------------------------------------------------------------------------------------------------------------|

            //Four accepted versions of commands
            GrammarBuilder commandGrammarB = new GrammarBuilder();          //accepts command only - no prefix or parameter diction
            GrammarBuilder commandPreGrammarB = new GrammarBuilder();       //accepts commands with prefixes (like open or run)
            GrammarBuilder commandParGrammarB = new GrammarBuilder();       //accepts commands with free diction parameters
            GrammarBuilder commandPreParGrammarB = new GrammarBuilder();    //accepts commands with prefixes and free diction parameters
            commandGrammarB.Culture = usCulture;
            commandPreGrammarB.Culture = usCulture;
            commandParGrammarB.Culture = usCulture;
            commandPreParGrammarB.Culture = usCulture;


            //One partial command grammar
            GrammarBuilder partialGrammarB = new GrammarBuilder();
            partialGrammarB.Culture = usCulture;
            //Only in case of greeting but nothing much else

            //DO STUFF HERE


            //And four new versions of commands without name addressing
            GrammarBuilder familiarCommandGrammarB = new GrammarBuilder();
            GrammarBuilder familiarCommandPreGrammarB = new GrammarBuilder();
            GrammarBuilder familiarCommandParGrammarB = new GrammarBuilder();
            GrammarBuilder familiarCommandPreParGrammarB = new GrammarBuilder();

            List<GrammarBuilder> commandGBList = new List<GrammarBuilder>() {greetingOpt, name, politenessPrefixOpt, command, politenessPostfixOpt};
            List<GrammarBuilder> commandPreGBList = new List<GrammarBuilder>() {greetingOpt, name, politenessPrefixOpt, cmdPrefix, articleOpt, commandPre, politenessPostfixOpt};
            List<GrammarBuilder> commandParGBList = new List<GrammarBuilder>() {greetingOpt, name, politenessPrefixOpt, commandPar}; //needs diction and postfix
            List<GrammarBuilder> commandPreParGBList = new List<GrammarBuilder>() {greetingOpt, name, politenessPrefixOpt, cmdPrefix, articleOpt, commandPrePar}; //needs diction and postfix
            familiarCommandGrammarB.Culture = usCulture;
            familiarCommandPreGrammarB.Culture = usCulture;
            familiarCommandParGrammarB.Culture = usCulture;
            familiarCommandPreParGrammarB.Culture = usCulture;

            //Build final grammarbuilders from pieces
            grammarBuilderAdditions(ref commandGrammarB, commandGBList);
            grammarBuilderAdditions(ref commandPreGrammarB, commandPreGBList);
            grammarBuilderAdditions(ref commandParGrammarB, commandParGBList);
            commandParGrammarB.AppendDictation();
            commandParGrammarB.Append(politenessPostfixOpt);
            grammarBuilderAdditions(ref commandPreParGrammarB, commandPreParGBList);
            commandPreParGrammarB.AppendDictation();
            commandPreParGrammarB.Append(politenessPostfixOpt);


            //Final grammar initializations
            commandGrammar = new Grammar(commandGrammarB);
            commandPreGrammar = new Grammar(commandPreGrammarB);
            commandParGrammar = new Grammar(commandParGrammarB);
            commandPreParGrammar = new Grammar(commandPreParGrammarB);
            commandGrammar.Name = "Full Command - No prefix or parameters";
            commandPreGrammar.Name = "Full Command - Prefix but no parameters";
            commandParGrammar.Name = "Full Command - No prefix but parameters";
            commandPreParGrammar.Name = "Full Command - Prefix and parameters";
            /*familiarCommandGrammar = new Grammar(familiarCommandGrammarB);
            familiarCommandPreGrammar = new Grammar(familiarCommandPreGrammarB);
            familiarCommandParGrammar = new Grammar(familiarCommandParGrammarB);
            familiarCommandPreParGrammar = new Grammar(familiarCommandPreParGrammarB);*/

            List<Grammar> allRegularGrammars = new List<Grammar>() {commandGrammar, commandPreGrammar, commandParGrammar, commandPreParGrammar};
           // List<Grammar> allFamiliarGrammars = new List<Grammar>() {familiarCommandGrammar, familiarCommandPreGrammar, familiarCommandParGrammar, familiarCommandPreParGrammar};

            //Load regular grammar -UNTESTED, NEED PARTIAL GRAMMAR
            loadGrammars(allRegularGrammars);





            //Event Handling set-up
            sre.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(sre_SpeechRecognized);
            sre.SpeechDetected += new EventHandler<SpeechDetectedEventArgs>(sre_SpeechDetected);
            sre.SpeechHypothesized += new EventHandler<SpeechHypothesizedEventArgs>(sre_SpeechHypothesized);
            sre.SpeechRecognitionRejected += new EventHandler<SpeechRecognitionRejectedEventArgs>(sre_SpeechRecognitionRejected);

            //Adjust confidence settings to eliminate false positives
            sre.UpdateRecognizerSetting("CFGConfidenceRejectionThreshold", 75);
        }

        private void SpeechControlForm_Shown(object sender, EventArgs e) {
            //ListSettings(sre);
            // Start recognition.
            Console.WriteLine("Beginning recognition...");
            sre.RecognizeAsync(RecognizeMode.Multiple);

            string[] allCommands = commandTable.Keys.ToArray();
            CommandTextBox.Lines = allCommands;
        }

        // Create a simple handler for the SpeechRecognized event.
        public void sre_SpeechRecognized(object sender, SpeechRecognizedEventArgs e) {
            Console.WriteLine("Speech recognized: " + e.Result.Text + " - " + e.Result.Confidence + " | " + e.Result.Grammar.Name);
            //ss.SpeakAsync("You just said: " + e.Result.Text);

            //Call response function based on which grammar and pass result
            if (e.Result.Grammar.Name.IndexOf("Full Command") >= 0) {
                Console.WriteLine("Full Command Entered");
                fullCommandResponse(e.Result);
            } else if (e.Result.Grammar.Name.IndexOf("Partial Command") >= 0) {
                Console.WriteLine("Partial Command Entered");
                partialCommandResponse(e.Result);
            } else if (e.Result.Grammar.Name.IndexOf("Postfix Command") >= 0) {
                Console.WriteLine("Postfix Command Entered");
                postfixCommandResponse(e.Result);
            }
        }
        public void sre_SpeechDetected(object sender, SpeechDetectedEventArgs e) {
            Console.WriteLine("Speech detected");
        }
        public void sre_SpeechHypothesized(object sender, SpeechHypothesizedEventArgs e) {
            Console.WriteLine("Speech hypothesized");
        }
        public void sre_SpeechRecognitionRejected(object sender, SpeechRecognitionRejectedEventArgs e) {
            Console.WriteLine("Speech rejected: " + e.Result.Alternates + " | " + e.Result.Confidence);
        }

        async void delayedGrammarUnload(Grammar g, int waitT) {
            //Times out waiting for a postfix grammar command
            Console.WriteLine("Starting postfix acceptance countdown");
            await Task.Delay(waitT * 1000);

            if(g.Loaded)
                sre.UnloadGrammar(g); //No obvious problems if unloaded during recognition process
            Console.WriteLine("Postfix acceptance countdown ended");
        }

        //Deprecated
        async void runCommandExecutable(string cmd, string param, string fileLoc) {
            Process p = new Process() ;
            string line;
            string exe = "";
            bool paramSwitch = false;

            StreamReader file = new StreamReader(fileLoc);
            while ((line = file.ReadLine()) != null) {
                //Console.WriteLine(line + " (" + line.Length + ") | " + cmd + " (" + cmd.Length + ")");
                //Assumes a three line structure - 1. cmd name 2. include param or not 3. exe name
                if(cmd == line) {
                    if(Int32.Parse(line = file.ReadLine()) > 0) { paramSwitch = true; }
                    exe = file.ReadLine();
                    break;
                } else {
                    file.ReadLine();
                    file.ReadLine();
                }
            }

            p.EnableRaisingEvents = true;
            p.Exited += new EventHandler(p_watchExitCode);

            param = "\"" + param + "\"";
            if (paramSwitch) {
                Console.WriteLine(exe + " | " + param);
                p = Process.Start(@"..\..\..\..\..\Command Scripts\" + exe, param);
            } else {
                Console.WriteLine(exe);
                p = Process.Start(@"..\..\..\..\..\Command Scripts\" + exe);
            }
            return;
        }

        async void runCommandExecutableFromCmdTable(string cmd, string param) {
            Process p = new Process() ;
            string exe = "";
            bool paramSwitch = false;

            exe = commandTable[cmd]["script"];
            if(commandTable[cmd]["param"] == "1") {paramSwitch = true;}

            p.EnableRaisingEvents = true;
            p.Exited += new EventHandler(p_watchExitCode);

            param = "\"" + param + "\"";
            if (paramSwitch) {
                Console.WriteLine(exe + " | " + param);
                try {
                    p = Process.Start(@"..\..\..\..\..\Command Scripts\" + exe, param);
                } catch(Exception e) {
                    Console.WriteLine("Error attempting to run command exe: " + e.Message);
                }
            } else {
                Console.WriteLine("Starting: "+ exe);
                try {
                    p = Process.Start(@"..\..\..\..\..\Command Scripts\" + exe);
                } catch(Exception e) {
                    Console.WriteLine("Error attempting to run command exe with parameter: " + e.Message);
                }
            }
            return;
        }

        void separateCommandStrings() {
            List<string> commandStrList = new List<string>();
            List<string> commandStrWithPrefixList = new List<string>();
            List<string> commandStrWithParamList = new List<string>();
            List<string> commandStrWithPrefixParamList = new List<string>();

            foreach(KeyValuePair<string, Dictionary<string,string>> entry in commandTable) {

                if (entry.Value["prefix"] == "1" && entry.Value["param"] == "0") {
                    commandStrWithPrefixList.Add(entry.Key);
                } else if (entry.Value["prefix"] == "0" && entry.Value["param"] == "1") {
                    commandStrWithParamList.Add(entry.Key);
                } else if (entry.Value["prefix"] == "1" && entry.Value["param"] == "1") {
                    commandStrWithPrefixParamList.Add(entry.Key);
                } else {
                    commandStrList.Add(entry.Key);
                }
            }

            commandStr = commandStrList.ToArray();
            commandStrWithPrefix = commandStrWithPrefixList.ToArray();
            commandStrWithParam = commandStrWithParamList.ToArray();
            commandStrWithPrefixParam = commandStrWithPrefixParamList.ToArray();
            return;
        }


        void grammarBuilderAdditions(ref GrammarBuilder gb, List<GrammarBuilder> gbList) {
            foreach(GrammarBuilder subGb in gbList) {
                gb.Append(subGb);
            }
        }

        void loadGrammars(List<Grammar> gList) {
            foreach(Grammar g in gList) {
                sre.LoadGrammar(g);
            }
        }

        public void p_watchExitCode(object sender, EventArgs e) {
            Process p = (Process)sender;

            if (p.ExitCode > 0) {
                //Speak executable error code
                ss.SpeakAsync("Command executable failure. Error code: " + p.ExitCode);
            } else {
                //Ran perfectly, do nothing
            }
        }

        private static void ListSettings(SpeechRecognitionEngine recognizer) {
            foreach (string setting in settings) { //THANK YOU, RANDOM INTERNET STRANGER
                try {
                    object value = recognizer.QueryRecognizerSetting(setting);
                    Console.WriteLine("  {0,-30} = {1}", setting, value);
                } catch {
                    Console.WriteLine("  {0,-30} is not supported by this recognizer.",
                        setting);
                }
            }
            Console.WriteLine();
        }

        private void OnOffSwitch_Click(object sender, EventArgs e) {
            if (OnOffSwitch.Text == "Pause") {
                OnOffSwitch.Text = "Resume";

                sre.RecognizeAsyncStop();
            } else {
                OnOffSwitch.Text = "Pause";
                sre.RecognizeAsync();
            }
        }
        private void Close_Click(object sender, EventArgs e) {
            trayIcon.Icon = null;
            Close();
        }
        private void SpeechControlForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            trayIcon.Visible = false;
            trayIcon.Icon = null;
        }

        void changeColor(object sender, EventArgs e) {
            this.BackColor = Color.AliceBlue;
            this.Refresh();


        }

    }
}
