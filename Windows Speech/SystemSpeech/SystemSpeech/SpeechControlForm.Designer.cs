﻿namespace SystemSpeech {
    partial class SpeechControlForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpeechControlForm));
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.Close = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.OnOffSwitch = new System.Windows.Forms.Button();
            this.CommandTextBox = new System.Windows.Forms.RichTextBox();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.SuspendLayout();
            // 
            // trayIcon
            // 
            this.trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIcon.Icon")));
            this.trayIcon.Text = "Minerva Voice Control";
            this.trayIcon.Visible = true;
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(253, 0);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(30, 23);
            this.Close.TabIndex = 0;
            this.Close.Text = "X";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Minerva Voice Control";
            // 
            // OnOffSwitch
            // 
            this.OnOffSwitch.Location = new System.Drawing.Point(16, 37);
            this.OnOffSwitch.Name = "OnOffSwitch";
            this.OnOffSwitch.Size = new System.Drawing.Size(75, 23);
            this.OnOffSwitch.TabIndex = 2;
            this.OnOffSwitch.Text = "Pause";
            this.OnOffSwitch.UseVisualStyleBackColor = true;
            this.OnOffSwitch.Click += new System.EventHandler(this.OnOffSwitch_Click);
            // 
            // CommandTextBox
            // 
            this.CommandTextBox.Location = new System.Drawing.Point(110, 37);
            this.CommandTextBox.Name = "CommandTextBox";
            this.CommandTextBox.Size = new System.Drawing.Size(135, 200);
            this.CommandTextBox.TabIndex = 3;
            this.CommandTextBox.Text = "";
            // 
            // elementHost1
            // 
            this.elementHost1.AccessibleName = "MinervaVisualHost";
            this.elementHost1.Location = new System.Drawing.Point(16, 103);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(73, 74);
            this.elementHost1.TabIndex = 4;
            this.elementHost1.Text = "MinervaVisualHost";
            this.elementHost1.Child = null;
            // 
            // SpeechControlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.ControlBox = false;
            this.Controls.Add(this.elementHost1);
            this.Controls.Add(this.CommandTextBox);
            this.Controls.Add(this.OnOffSwitch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Close);
            this.Name = "SpeechControlForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SpeechControlForm_FormClosing);
            this.Load += new System.EventHandler(this.SpeechControlForm_Load);
            this.Shown += new System.EventHandler(this.SpeechControlForm_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon trayIcon;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button OnOffSwitch;
        private System.Windows.Forms.RichTextBox CommandTextBox;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
    }
}

