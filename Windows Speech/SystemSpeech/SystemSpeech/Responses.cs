﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Speech;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.IO;
using System.Diagnostics;
using System.Runtime.Remoting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SystemSpeech
{

    public partial class SpeechControlForm : Form {

        //Full Command Response - complete and correct given command, with possible wrong dictated parameters
        public void fullCommandResponse(RecognitionResult cmdResult) {
            string cmdText = cmdResult.Text;
            string resultCmd = "";
            string resultParam = "";
            string[] selectedCommandList = null;

            switch(cmdResult.Grammar.Name) {
                case "Full Command - No prefix or parameters":
                    selectedCommandList = commandStr;
                    break;
                case "Full Command - Prefix but no parameters":
                    selectedCommandList = commandStrWithPrefix;
                    break;
                case "Full Command - No prefix but parameters":
                    selectedCommandList = commandStrWithParam;
                    break;
                case "Full Command - Prefix and parameters":
                    selectedCommandList = commandStrWithPrefixParam;
                    break;
                default:
                    selectedCommandList = commandStr;
                    break;
            }
            
            //Find command in result and remove all preceding characters
            foreach (string substring in selectedCommandList) {
                int index = cmdText.IndexOf(substring);
                if (index < 0) {
                    if (substring == selectedCommandList[selectedCommandList.Length-1]) {
                        //No correct command in result, return speech error
                        Console.WriteLine("Command failure: error number 43");
                        ss.SpeakAsync("Command failure: error number 43");
                        return;
                    }
                } else { //Given command is correct
                    resultCmd = substring;
                    cmdText = cmdText.Remove(0, index); //Remove all characters up to Command phrase
                    break;
                }
            }
            //Remove trailing politeness phrase
            foreach (string substring in politenessPostfixStr) {
                int index = cmdText.IndexOf(substring);
                if (index < 0) {
                    //Do nothing
                } else {//Remove politness phrase
                    cmdText = cmdText.Remove(index, substring.Length); //Remove all characters up to Command phrase
                    break;
                }
            }

            cmdText = cmdText.Trim();
            resultParam = cmdText.Remove(0, resultCmd.Length);
            resultParam = resultParam.Trim();

            //Run autohotkey script based on resultCmd and resultParam
            Console.WriteLine(resultCmd);
            Console.WriteLine(resultParam);

            //runCommandExecutable(resultCmd, resultParam, @"..\..\..\..\..\Command Scripts\config\cmdList.ini");
            runCommandExecutableFromCmdTable(resultCmd, resultParam);
        }

        public void partialCommandResponse(RecognitionResult cmdResult) {
            //Load postfix command grammar
           // sre.LoadGrammar(postfixG);
            if(!postfixG.Loaded)
                sre.LoadGrammarAsync(postfixG);

            string cmdText = cmdResult.Text;
            bool brokenCmd = false; //Assumes "Hey Minerva" or some variation without a command prefix or command

            foreach (string substring in cmdPrefixStr) {
                int index = cmdText.IndexOf(substring);
                if (index < 0) {
                    //Do nothing
                } else {//Finds command prefix
                    brokenCmd = true;
                    break;
                }
            }

            if (!brokenCmd) {
                foreach (string substring in commandStr) {
                    int index = cmdText.IndexOf(substring);
                    if (index < 0) {
                        //Do nothing
                    } else {//Finds command
                        brokenCmd = true;
                        break;
                    }
                }
            }

            if (brokenCmd) { clarificationSynth(); } else { initializationSynth(); }

            //Reset timer to unload postfix grammar
            delayedGrammarUnload(postfixG, 8); //Waits 8 seconds and unloads postfix grammar
        }

        public void postfixCommandResponse(RecognitionResult cmdResult) {
            //Lol do I even need this
            fullCommandResponse(cmdResult);
        }


        //Synth responses to a voice input
        public void initializationSynth() {
            //Picks a random response from initialization file
            //Maybe replace with notification sound like Siri or Google Now?
            ss.SpeakAsync("Yes?");
        }

        public void clarificationSynth() {
            //Picks a random response from clarification file
            ss.SpeakAsync("Say again?");
        }

        public void affirmationSynth() {
            //Picks a random response from affirmation file
            //Also recites given command?
            ss.SpeakAsync("Doing that thing you told me to do.");
        }
    

    }
}
