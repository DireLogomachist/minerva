﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Speech;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.IO;
using System.Diagnostics;
using System.Runtime.Remoting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SystemSpeech
{
    public partial class SpeechControlForm : Form {

        string[] stringArrayFromFile(string fileLoc) {
            List<string> list= new List<string>();
            string line;

            StreamReader file = new StreamReader(fileLoc);
            while ((line = file.ReadLine()) != null) {
                list.Add(line);
            }

            return list.ToArray();
        }

        string[] stringArrayFromJsonDict(string fileLoc) {
            List<string> list= new List<string>();

            JObject o = JObject.Parse(File.ReadAllText(fileLoc));

            foreach (var item in o)
            {
                string name = item.Key;
                JToken value = item.Value;
                //Console.WriteLine(name);
                //Console.WriteLine(value["script"]);
                //Console.WriteLine(value["param"]);

                list.Add(name);
            }

            return list.ToArray();
        }

        string[] stringArrayFromJsonList(string fileLoc) {
            List<string> list = new List<string>();

            JArray o = JArray.Parse(File.ReadAllText(fileLoc));

            //Console.WriteLine(o.Count);

            foreach (var item in o.Children())
            {
                string name = item.ToString(); //item is of class JProperty
                list.Add(name);
            }

            return list.ToArray();
        }

        Dictionary<string, Dictionary<string, string>> buildCommandTable(string fileLoc) {
            Dictionary<string, Dictionary<string, string>> cmdTable = new Dictionary<string,Dictionary<string,string>>();

            string a = File.ReadAllText(fileLoc);

            JObject o = JObject.Parse(File.ReadAllText(fileLoc));
            foreach (var item in o)
            {
                Dictionary<string, string> entry = new Dictionary<string,string>();
                JToken value = item.Value;

                string name = item.Key;
                string param = value["param"].ToString();
                string prefix = value["prefix"].ToString();
                string script = value["script"].ToString();
                
                entry["script"] = script;
                entry["param"] = param;
                entry["prefix"] = prefix;

                cmdTable[name] = entry;
            }
            return cmdTable;
        }

    }
}