﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Speech;
using Microsoft.Speech.Recognition;
using Microsoft.Speech.Synthesis;


namespace SpeechControlProgram {
    public partial class MainForm : Form {

        SpeechRecognitionEngine sre;
        SpeechSynthesizer ss;

        string _ZiraPro = "Microsoft Server Speech Text to Speech Voice (en-US, ZiraPro)";  //High quality
        string _Hazel = "Microsoft Server Speech Text to Speech Voice (en-GB, Hazel)";      //High quality
        string _Helen = "Microsoft Server Speech Text to Speech Voice (en-US, Helen)";      //Mid quality
        string _Heather = "Microsoft Server Speech Text to Speech Voice (en-CA, Heather)";  //Terrible quality
        string _Hayley = "Microsoft Server Speech Text to Speech Voice (en-AU, Hayley)";    //Terrible quality
        string _Heera = "Microsoft Server Speech Text to Speech Voice (en-IN, Heera)";    //Terrible quality

        static readonly string[] settings = new string[] {
            "ResourceUsage",
            "ResponseSpeed",
            "ComplexResponseSpeed",
            "AdaptationOn",
            "PersistedBackgroundAdaptation",
            "CFGConfidenceRejectionThreshold",
            "HighConfidenceThreshold", 
            "NormalConfidenceThreshold",
            "LowConfidenceThreshold"
        };

        public MainForm() {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e) {
            sre = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("en-US"));
            ss = new SpeechSynthesizer();
            ss.SelectVoice(_ZiraPro);

            //Congigure recognizer and synthesizer
            sre.SetInputToDefaultAudioDevice();
            ss.SetOutputToDefaultAudioDevice();

            Choices colors = new Choices();
            colors.Add(new string[] {
                "red",
                "green",
                "blue",
                "Jarvis",
                "Cortana",
                "steam game",
                "shining force",
                "ketesh",
                "Minerva",
                "Cleo"
            });

            GrammarBuilder gb = new GrammarBuilder();
            gb.Append(colors);

            Grammar g = new Grammar(gb);
            sre.LoadGrammar(g);

            //Free-speech dictation
            
            
            
            sre.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(sre_SpeechRecognized);
            sre.SpeechDetected += new EventHandler<SpeechDetectedEventArgs>(sre_SpeechDetected);
            sre.SpeechHypothesized += new EventHandler<SpeechHypothesizedEventArgs>(sre_SpeechHypothesized);
            sre.SpeechRecognitionRejected += new EventHandler<SpeechRecognitionRejectedEventArgs>(sre_SpeechRecognitionRejected);


            //Adjust confidence settings to eliminate false positives
            sre.UpdateRecognizerSetting("CFGConfidenceRejectionThreshold", 75);
            
        }

        private void MainForm_Shown(object sender, EventArgs e) {
            ListSettings(sre);

            // Start recognition.
            Console.WriteLine("Beginning recognition...");
            sre.RecognizeAsync(RecognizeMode.Multiple);
        }

        // Create a simple handler for the SpeechRecognized event.
        public void sre_SpeechRecognized(object sender, SpeechRecognizedEventArgs e) {
            Console.WriteLine("Speech recognized: " + e.Result.Text + " - " + e.Result.Confidence);
            
            switch(e.Result.Text) 
            {
                case "Minerva":
                    ss.SelectVoice(_Hazel);
                    break;
                case "Cleo":
                    ss.SelectVoice(_ZiraPro);
                    break;
                case "Cortana":
                    ss.SelectVoice(_Helen);
                    break;
            }
            ss.Speak("You just said: " + e.Result.Text);


            //TESTING FREE-SPEECH DICTATION
            sre.UnloadAllGrammars();

            GrammarBuilder startStop = new GrammarBuilder();
            GrammarBuilder dictation = new GrammarBuilder();
            //dictation.AppendDictation();
 
            startStop.Append(new SemanticResultKey("StartDictation", new SemanticResultValue("Start Dictation", true)));
            //startStop.Append(new SemanticResultKey("DictationInput", dictation));
            startStop.Append(new SemanticResultKey("StopDictation", new SemanticResultValue("Stop Dictation", false)));
            Grammar gr = new Grammar(startStop);
            gr.Enabled = true;
            gr.Name = " Free-Text Dictation ";
            sre.LoadGrammar(gr);

        }

        public void sre_SpeechDetected(object sender, SpeechDetectedEventArgs e) {
            Console.WriteLine("Speech detected");
        }

        public void sre_SpeechHypothesized(object sender, SpeechHypothesizedEventArgs e) {
            Console.WriteLine("Speech hypothesized");
        }

        public void sre_SpeechRecognitionRejected(object sender, SpeechRecognitionRejectedEventArgs e) {
            Console.WriteLine("Speech rejected: " + e.Result.Alternates);
        }

        //THANK YOU, RANDOM INTERNET STRANGER
        private static void ListSettings(SpeechRecognitionEngine recognizer)
        {
            foreach (string setting in settings)
            {
                try
                {
                    object value = recognizer.QueryRecognizerSetting(setting);
                    Console.WriteLine("  {0,-30} = {1}", setting, value);
                }
                catch
                {
                    Console.WriteLine("  {0,-30} is not supported by this recognizer.",
                      setting);
                }
            }
            Console.WriteLine();
        }
    }

}
