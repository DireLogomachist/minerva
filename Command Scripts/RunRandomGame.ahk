﻿; Steam Randomizer v1 - Launches a random game from the installed games in a Steam Library. 
; Stefan Jones - August 2014
;
; Usage: On first startup, select a Steam Library folder that you wish to load games from. Afterwards, just run the script to launch a random game.
; Known Bugs: This initial version simply opens an 'app' from the steamapps folder, which in some cases, may not actually be a game. 
;             The script doesn't make a distinction between apps and games, so it may launch an app instead.

SetWorkingDir %A_ScriptDir%

#NoEnv			; Recommended for performance and compatibility with future AutoHotkey releases. Avoids checking empty variables to see if they are environment variables.
SendMode Input		; Recommended for new scripts due to its superior speed and reliability.
#SingleInstance force

Steam_Data = config\steamfolder.ini ; Store persistent settings at this location.
;Steam_Data = steamfolder.ini

if !FileExist(Steam_Data) {
	FileSelectFolder, Folder, , 0, Provide the path to your main Steam Folder (e.g. C:\Program Files\Steam). You can also choose a Steam Library to select games from there.
	if Folder =
		exit

	Steam_Dir := RegExReplace(Folder, "\\$")  ; Removes the trailing backslash, if present.
	Steam_Dir = %Steam_Dir%\steamapps

	if FileExist(Steam_Dir) {
		FileDelete, %Steam_Data%
		FileAppend, %Steam_Dir%, %Steam_Data%
		if ErrorLevel   ; i.e. it's not blank or zero.
			MsgBox, File could not be written. AutoHotKey returned an error code of %ErrorLevel%
	} 
	else {
		MsgBox, 0, Steam Library Not Found, There are no games installed at %Steam_Dir%. This is not a valid Steam Library or Steam Folder. Please run this script again to select a new location.
		exit
	}
}
if FileExist(Steam_Data) {
	FileReadLine, Steam_Dir, %Steam_Data%, 1 ; First Line contains the Steam Library location
	
	if FileExist(Steam_Dir) {
		ArrayCount = 0
	
		; Installed games and software have an .acf file listed in the SteamApps folder.
		; Create an Array list of these filenames
		Loop, %Steam_Dir%\appmanifest_*.acf, 0, 0 
		{	
			ArrayCount += 1
			Array%ArrayCount% := A_LoopFileName ; Store the filename in the next array element.
		}
		
		; Something has gone wrong with your Steam Library
		if (ArrayCount == 0) {
			MsgBox, No games were found at this location.
			FileDelete, %Steam_Data%	
			return
		}

		; Save a random integer smaller than the number of games to Rand_Index, and pick that index to play
		Random, Rand_Index, 1, (ArrayCount) 
		Selected_File := Array%Rand_Index%
		
		; Parse the file name. The magic number 12 is the length of the prefix 'appmanifest_'. 	We want the numeric portion of the file.
		Found_Period_Pos := RegExMatch(Selected_File, "\.", "12")
		Game_Number := SubStr(Selected_File, 13, Found_Period_Pos - 13)

		; Launch Steam with GameID syntax, like a desktop shortcut
		Game_URL = steam://rungameid/%Game_Number%
		
		; GUI for launch confirmation
		Gui +Resize -MaximizeBox
		Gui, Font, s12
		Gui, Add, Text, , Launch a game from your Library?
		Gui, Font, s10
		Gui, Add, Button, w100, OK
		Gui, Add, Button, xp+140 wp, Cancel
		Gui, Show, AutoSize Center,Ready to Launch
		GuiControl, +default, OK

		return
	}
	; Start from scratch in the case of an invalid folder
	else {
		FileDelete, %Steam_Data%
		MsgBox, 0x30, Steam Library Not Found, A Steam library was not found at '%Steam_Dir%'. Please re-run this script to select a new library location.
	}	
}
ExitApp

ButtonOK:
	Run, %Game_URL%
	MsgBox, 64, Game starting..., This window will close automatically, 2 
ButtonCancel:
	ExitApp
GuiClose:
	ExitApp