#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn, LocalSameAsGlobal, Off  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force


;Separate parameter words with + and append to google search url
searchParams = %1%
urlstring := " https://www.google.com/search?q="
StringReplace, searchParams, searchParams, %A_SPACE%, +, All

if WinExist("ahk_class MozillaWindowClass") {
	firefoxExists = true
	urlstring := "-new-tab " . urlstring
} else {
	firefoxExists = false
}

urlstring := urlstring . searchParams

;Run firefox (if doesn't already exist) with formulated google search url, bring to focus and fullscreen
SetTitleMatchMode, 2

run firefox.exe %urlstring%

if firefoxExists = false
	winwaitactive, Mozilla Firefox
WinMaximize, Mozilla Firefox

Return