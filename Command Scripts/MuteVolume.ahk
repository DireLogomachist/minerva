#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force

SoundGet, muteStatus, , mute
;MsgBox, Master Mute is currently %muteStatus%

if muteStatus = Off
{
	Send {Volume_Mute}
	TrayTip, Minerva, Volume muted, 10, 1
	sleep 10000
}


Return