﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force

/*  STEAM DATA NOTES -------------------------------
	Steam stores installed games as appmanifest_******.acf files in C:\Program Files (x86)\Steam\SteamApps
	Non-steam games are stored in C:\Program Files (x86)\Steam\userdata\107086185\config\shortcuts.vdf (kinda unreadable)
	All games id's (installed and not) and their categories are stored in C:\Program Files (x86)\Steam\userdata\107086185\7\remote\sharedconfig.vdf
*/
/*
MAIN PROGRAM EXECUTION
*/
;MAIN VARIABLES
gameIdConfig := "C:\Users\MILLERAM1.2CE12909KN\Documents\DEVICE 2CE1\Code Projects and Tools\Project 1052\Command Scripts\config\gameIdList.ini"
gameIdJsonConfig := "C:\Users\MILLERAM1.2CE12909KN\Documents\DEVICE 2CE1\Code Projects and Tools\Project 1052\Command Scripts\config\gameIdList.json" ;not currently used...
steamAppsFolder := "C:\Program Files (x86)\Steam\SteamApps"


;^j::
;Launch and maximize steam
SetTitleMatchMode, RegEx
IfWinExist Steam;ahk_class ^USurface_\d*
{
	WinGet,WinState,MinMax,ahk_class ^USurface_\d*
	If WinState < 1
	{
		WinMaximize
		WinActivate
		;MsgBox, Not maximized
	} else 
	{
		;MsgBox, Maximized
	}
	
} else
{
	Run, C:\Program Files (x86)\Steam\Steam.exe, , max
	WinWait, Steam
	
	SetTitleMatchMode, 3
	WinGet,WinState,MinMax,ahk_class ^USurface_\d*
	If WinState < 1
	{
		WinActivate, Steam
		WinMaximize, Steam
		;MsgBox, Not maximized-fromNot
	} else 
	{
		;MsgBox, Maximized-fromNot
	}
	
	;MsgBox, No Steam detected
}


;LAUNCH GAME IF NAME PASSED IN AS COMMAND LINE ARGUMENT
If %0% = 0
{
	;MsgBox, No command line args
} else
{
	;MsgBox, game Name: %1%
	
	;Parse game title from command line arg - DONE
	;Create array of potential name subs - (subbing two for II, etc.) - DONE
	;Search gameIdList.config for name matches
	;If found, run game by id using steam browser protocol and return
	;If not, gather list of id's in gameIdList, then gather list of all appmanifest files that do not contain id in name
	;	If any left, search text file for name matches
	;		If found, then add to matching name and id to gameIdList and run game by id
	;		If not, then return game-not-found voice error
	;	If none left, then return game-not-found voice error
	;If no appmanifest files not in config, then return voice error
	
	givenName = %1%
	
	nameList := Array()
	nameList.Insert(givenName)
	wordArray := Array()
	wordArray := Split(givenName, " ")
	
	lastWord := wordArray[wordArray.Length()]
	StringLen, num, lastWord
	num++
	StringTrimRight, bulkName, givenName, %num%
	
	;GENERATES ALTERNATE NAME POSSIBILTIES
	If wordArray.Length() > 1
	{
		subList := [["one","I", "1"],["two", "II", "2"],["three", "III", "3"],["four", "IV", "4"],["five", "V", "5"],["six", "VI", "6"],["seven", "VII", "7"],["eight", "VIII", "8"],["nine", "IX", "9"],["ten", "X", "10"]]
		StringCaseSense, Off
		
		outerLoop:
		Loop % subList.Length()
		{
			mainLoop := A_Index
			
			Loop % subList[mainLoop].Length()
			{
				compare := subList[mainLoop][A_Index]
				
				;Match between a given subfix in array and lastWord
				IfInString, compare, %lastWord%
				{
					indexMatch := A_Index
					Loop % subList[mainLoop].Length()
					{
						If indexMatch != %A_Index%
						{
							newName := bulkName . " " . subList[mainLoop][A_Index]
							nameList.Push(newName)
						}
					}
					
					If mainLoop = 1
					{
						nameList.Push(bulkName)
					}
					
					break outerLoop
				}
			}
		}
	}
	
	;SEARCH CONFIG LIST FOR NAME MATCH
	found := false
	gameId := "0"

	Loop, read, %gameIdConfig%
	{
		If (found)
		{
			gameId := A_LoopReadLine
			break
		}
		
		secondInnerLoop:
		Loop % nameList.Length()
		{
			mainLoop := A_Index
			mainText := A_LoopReadLine
			name := nameList[mainLoop]
			IfEqual, mainText, %name%
			{
				found := true
				break secondInnerLoop
			}
		}
	}
	
	If (!found)
	{
		;Gather all recorded gameids from config and get list of all appmanifest files
		gameIdArray := Array()
		manifestArray := Array()
		parsedNameArray := Array()
		
		Loop, read, %gameIdConfig%
		{
			modTemp := Mod(A_Index, 2)
			If modTemp = 0
			{
				gameIdTemp := A_LoopReadLine
				
				;Didn't help...
				StringRight, lastChar, gameIdTemp, 1
				If lastChar = "`r" || lastChar = "`n"
				{
					StringTrimRight, gameIdTemp, gameIdTemp , 1
				}
				
				gameIdArray.Insert(A_LoopReadLine)
			}
		}
		
		;Gather a list of all id numbers listed in the names of appmanifest files
		Loop, %steamAppsFolder%\appmanifest_*.acf, 0, 0
		{
			manifestTemp := A_LoopFileName
			StringTrimRight, manifestTemp, manifestTemp , 4
			StringTrimLeft, manifestTemp, manifestTemp, 12
			manifestArray.Insert(manifestTemp)
		}
		
		;Remove manifests recorded already in gameIdList.ini
		manDeleteList := Array()
		Loop % manifestArray.Length()
		{
			manifestLoop := A_Index
			Loop % gameIdArray.Length()
			{
				If manifestArray[manifestLoop] = gameIdArray[A_Index]
				{
					;Break takes care of duplicates
					manDeleteList.Insert(manifestLoop)
					break
				} else
				{
					;No match - do nothing
				}
			}
		}
		
		;Replace duplicates with empty strings
		Loop % manDeleteList.Length()
		{
			deleteNum := manDeleteList[A_Index]
			manifestArray.Delete(deleteNum)
			;MsgBox Deleting %deleteNum%
		}
		
		;Remove empty entries from manifestArray
		counter := manifestArray.Length()
		inc := 1
		While inc <= counter
		{
			if manifestArray[inc] = ""
			{
				manifestArray.RemoveAt(inc)
				counter--
				inc--
			}
			inc++
		}
		
		;MsgBox % manifestArray.Length() . " manifest(s) not in config file"
		
		;If no additional game id's found in appmanifest file names...
		If manifestArray.Length() = 0
		{
			;MsgBox % "Error 41: Steam game not found..."
			TrayTip, Minerva, Steam game %givenName% not found in Steam appmanifest, 10, 2
			sleep 10000
			;Return the program with an error code > 0
			;return 41 (?)
		} else
		{
			
			;Loop through unread manifest files and scrape names from each of them
			Loop % manifestArray.Length()
			{
				lineCount := 1
				manifestLoc := steamAppsFolder . "\appmanifest_" . manifestArray[A_Index] . ".acf"
				Loop, read, %manifestLoc%
				{
					If lineCount = 5
					{
						lineValue := A_LoopReadLine
						StringTrimLeft, lineValue, lineValue, 10
						StringTrimRight, lineValue, lineValue, 1
						parsedNameArray.Insert(lineValue)
						break
					}
					lineCount++
				}
			}
			
			;Add scraped names and gameID's to gameIdConfig and check list again
			idFile := FileOpen(gameIdConfig, "a")
			
			Loop % manifestArray.Length()
			{
				pTemp := parsedNameArray[A_Index]
				mTemp := manifestArray[A_Index]
				additionTemp := "`r" . "`n" . pTemp . "`r" . "`n" . mTemp
				idFile.Write(additionTemp)
			}
			
			;Search through parsedNameArray to find matching name
			Loop % parsedNameArray.Length()
			{
				mainLoop := A_Index
				If (found)
				{
					gameId := manifestArray[mainLoop]
					break
				}
				Loop % nameList.Length()
				{
					checkName := parsedNameArray[mainLoop]
					name := nameList[mainLoop]
					IfEqual, checkName, %name%
					{
						found := true
						break
					}
				}
			}
			
			If (found)
			{
				;MsgBox Running Steam game -%givenName%
				runCmd = steam://rungameid/%gameId%
				Run, %runCmd%
			} else {
				;MsgBox % "Error 42: Steam game not found"
				TrayTip, Minerva, Unable to find steam game %givenName%, 10, 2
				sleep 10000
				;Return the program with an error code > 0
				;return 42 (?)
			}
		}
	} else
	{
		;WORKS!!!
		;MsgBox Running Steam game -%givenName%
		runCmd = steam://rungameid/%gameId%
		Run, %runCmd%

		TrayTip, Minerva, Running steam game %givenName% , 10, 1
		sleep 10000
	}
	
	
	
}

;Thank you, random internet stranger!
Split(in,delim,omit="") {

	StringSplit, out, in, %delim%, %omit%
	o :=	[]
	Loop %	out0
		o.Insert(out%A_Index%)
	return	o
}

Return
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	