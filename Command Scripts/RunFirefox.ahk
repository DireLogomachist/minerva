#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force


;Run firefox (if doesn't already exist), bring to focus and fullscreen
SetTitleMatchMode, 2

if !WinExist("ahk_class MozillaWindowClass") {
	run firefox.exe ;new-tab www.google.com www.stackoverflow.com ;http://kb.mozillazine.org/Command_line_arguments
	winwaitactive, Mozilla Firefox
}
WinMaximize, Mozilla Firefox

Return