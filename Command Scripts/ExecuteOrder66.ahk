#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force

;Do something stupid, like turn off the lights or shut down the computer (shutdown wifi maybe?)
;Currently just locks the Screen - need something more diabolical

;Ok now changes logon screen to Emperor Palpatine
;Play Star Wars sound clip: "Yes, my lord!"
;Then locks the computer
;On login again it changes the logon screen back


SetTitleMatchMode, 2 ;needed? might mess things up
imageFile := "Palpatine.jpg"
soundFile := "Yes My Master.mp3"

;change logon screen - will work differently from Win7 to Win10

;for Win7:
;default image stored at C:\Windows\System32\oobe\info\backgrounds\backgroundDefault.jpg
;so place Palpatine.jpg (no bigger than 256KB) there as well
;rename current background image to backgroundDefault_Original.jpg
;and rename Palpatine.jpg to backgroundDefault.jpg
;should be all that is needed

;for Win10:
;no clue - totally different system
if (A_OSVersion = "WIN_7") {
	IfExist, C:\Windows\System32\oobe\info\backgrounds\backgroundDefault.jpg
	{
		IfExist, C:\Windows\System32\oobe\info\backgrounds\%imageFile%
		{
			FileMove, C:\Windows\System32\oobe\info\backgrounds\backgroundDefault.jpg, C:\Windows\System32\oobe\info\backgrounds\backgroundDefault_Original.jpg
			FileMove, C:\Windows\System32\oobe\info\backgrounds\%imageFile%, C:\Windows\System32\oobe\info\backgrounds\backgroundDefault_Original.jpg ;Unsure about syntax here - unquoted text makes me nervous and
		}
	}
}

if (SubStr(A_OSVersion, 1, 3) = "10.") {
	; for Win10 - call C# exe to set lock screen image, and record returned filename of original picture (how to handle if no pic used? unsure...)
}


IfExist, %A_ScriptDir%\..\Audio Responses\ExecuteOrder66\%soundFile%
{
	SoundPlay, %A_ScriptDir%\..\Audio Responses\ExecuteOrder66\%soundFile%
}

WinGetActiveTitle T
DllCall("LockWorkStation") ;or [Send #l]? Could do either but DllCall is proven to work correctly
Sleep 2000
WinWaitActive %T%

;change logon screen back

;for Win7:
;rename current backgroundDefault.jpg back to Palpatine.jpg
;rename backgroundDefault_TrueOriginal.jpg back to backgroundDefault.jpg
;and done
if (A_OSVersion = "WIN_7") {
	IfExist, C:\Windows\System32\oobe\info\backgrounds\backgroundDefault.jpg
	{
		IfExist, C:\Windows\System32\oobe\info\backgrounds\%imageFile%
		{
			FileMove, C:\Windows\System32\oobe\info\backgrounds\backgroundDefault.jpg, C:\Windows\System32\oobe\info\backgrounds\%imageFile% ;Unsure of syntax here...
			FileMove, C:\Windows\System32\oobe\info\backgrounds\backgroundDefault_Original.jpg, C:\Windows\System32\oobe\info\backgrounds\backgroundDefault.jpg
		}
	}
}

if (SubStr(A_OSVersion, 1, 3) = "10.") {
	; for Win10 - call C# exe to set lock screen image using previously returned filename
}

;Uh don't use this cmd it shuts down the computer
;Shutdown, 1

Return