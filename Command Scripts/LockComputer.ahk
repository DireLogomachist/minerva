#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force


;Just locks the computer and returns to login screen - does NOT log off the current session
DllCall("LockWorkStation")

;Uh don't use this it shuts down the computer
;Shutdown, 1

Return