#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn, LocalSameAsGlobal, Off  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force
#WinActivateForce

SetTitleMatchMode 2

;if font too small change in Defaults, not Properties
if !WinExist("ahk_class ConsoleWindowClass") {
	run cmd.exe
	winwaitactive, cmd.exe
} else {
	WinActivate, cmd.exe
	WinShow
}

WinGetPos,,, sizeX, sizeY
WinMove, (A_ScreenWidth/2)-(sizeX/2), (A_ScreenHeight/2)-(sizeY/2)
WinSet, Top
WinActivate

if 0 = 1
{
	param = %1%
	if (param = "full screen" || param = "fullscreen")
	{
		WinMaximize, cmd.exe
		TrayTip, Minerva, Starting fullscreen command console, 10, 1
		sleep 10000
	} else
	{
		TrayTip, Minerva, Starting command console`nUnable to parse additional parameter, 10, 2
		sleep 10000
	}
} else {
	TrayTip, Minerva, Starting command console, 10, 1
	sleep 10000
}




Return