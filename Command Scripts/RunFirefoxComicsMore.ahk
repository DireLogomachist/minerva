#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn, LocalSameAsGlobal, Off  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force

;#Include %A_ScriptDir%\lib\AHK_DBA-IsNull\Lib\SQLite_L.ahk
;#Include %A_ScriptDir%\lib\AHK_DBA-IsNull\Lib\DBA.ahk

#Include <DBA>

placesDB := A_AppData . "\Mozilla\Firefox\Profiles\t2qenwmk.default\places.sqlite" ;home laptop - eventually replace with string from config file
;placesDB := A_AppData . "\Mozilla\Firefox\Profiles\e9rfzts7.default\places.sqlite" ;work desktop - throws errro about sqlite dll being missing in x64 folder (there isn't one anymore...)
;placesDB := A_AppData . "\Mozilla\Firefox\Profiles\9epyos6x.default\places.sqlite"	;work laptop


;Match to given days
todayNum = %A_WDay%
yesterdayNum := todayNum - 1
if yesterdayNum = 0
	yesterdayNum = 7

weekArray := ["'Sunday'", "'Monday'", "'Tuesday'", "'Wednesday'", "'Thursday'", "'Friday'", "'Saturday'"]
weekDict := {"sunday":1, "monday":2, "tuesday":3, "wednesday":4, "thursday":5, "friday":6, "saturday":7, "'yesterday'":%todayNum%, "today":%yesterdayNum%}

dayString := weekArray[todayNum] ;arrays are 1 one based so no offset needed, Sunday starts at 1

;Gather urls from sqlite matching parent folder as day of week in comic folder
;connect to sqlite file
databaseType := "SQLite"
connectionString = %placesDB%
urlDict := {}
urlstring := ""


numArgs := 0
if(%numArgs% != 1) {
	TrayTip, Minerva, Wrong number of command line arguments, 10, 2
	sleep 10000
} else {
	inputParam = %1%
	StringLower, inputParam, inputParam

	;for on ---
	;between --- and ---
	;so far || so far this week || for so far this week || this week so far || for this week so far
	;this week || for this week

	if(RegExMatch(inputParam, "(for)?(on)? (sun|mon|tues|wednes|thurs|fri|satur|to|yester)day") > 0) {
		dayString := getComicsOn()
	} else if(RegExMatch(inputParam, "(for )?(between) (sun|mon|tues|wednes|thurs|fri|satur|to|yester)day and (sun|mon|tues|wednes|thurs|fri|satur|to|yester)day") > 0) {
		dayString := getComicsBetween()
	} else if(inputParam = "so far" || inputParam = "so far this week" || inputParam = "for so far this week" || inputParam = "this week so far" || inputParam = "for this week so far") {
		dayString := getComicsSoFar()
	} else if("this week" || "for this week") {
		dayString := getAllComics()
	} else {
		TrayTip, Minerva, Unable to get comics with input`n'%inputParam%', 10, 2
		sleep 10000
		return
	}


;return


	;Get the multiple parent days folder ids
	;Currently only grabs one, though functions will return several in a subquery string (Monday AND Tuesday AND Friday, etc.)
	;dayID needs to be switched to a similar format - 1123 OR 2123 OR 5563 - looping through rows to get them all

	try {
		currentDB := DBA.DataBaseFactory.OpenDataBase(databaseType, connectionString)
		try {
			getDayQuery := "SELECT id FROM moz_bookmarks WHERE parent IN (SELECT id FROM moz_bookmarks WHERE title IS 'Webcomic Schedule' LIMIT 1) AND title IS " . daystring

			table := currentDB.Query(getDayQuery)

			dayId := ""
			if(IsObject(table) and table.Rows.Count() > 0) {
				;dayId := table.Rows[1][1] ;old definition

				for each, row in table.Rows
				{
					if (dayId == "")
					{
						dayId := row[1]
					} else {
						dayId := dayId . " OR " . row[1]
					}
				}

				msgbox, dayId

			} else {
				TrayTip, Minerva, No comics bookmarked for given days, 10, 1
				sleep 10000
				return
			}
		} catch e {
			TrayTip, Minerva, Unable to query bookmarks from sqlite, 10, 2
			sleep 10000
			return
		}
	} catch e {
		msgbox % e.Message 
		TrayTip, Minerva, Unable to open Mozilla bookmarks sqlite, 10, 2
		sleep 10000
		return
	}

	if WinExist("ahk_class MozillaWindowClass") {
		firefoxExists = true
	} else {
		firefoxExists = false
	}

	try {
		;odd behavior: with existing window current tab is last tab opened, without it is the first tab opened, so order is reversed depending for consistency
		getURLs := "SELECT url FROM moz_places JOIN moz_bookmarks ON moz_places.id = moz_bookmarks.fk WHERE parent IS " . dayId . " ORDER BY position"

		if firefoxExists = true
			getUrls := getUrls . " DESC"

		table := currentDB.Query(getURLs)
		if(IsObject(table)  and table.Rows.Count() > 0) {
			for each, row in table.Rows
			{
				;Ignore duplicates of urls already in urlstring
				url := " " . row[1]
				IfNotInString, urlstring, %url%
				{
					if (%firefoxExists% = true)
					{
						urlstring := urlstring . " -new-tab " . row[1]
					} else
						urlstring := urlstring . " " . row[1]
				}
			}
		} else {
			TrayTip, Minerva, No comics bookmarked for today, 10, 1
			sleep 10000
			return
		}
	} catch e {
		TrayTip, Minerva, Unable to query bookmarks from sqlite, 10, 2
		sleep 10000
		return
	}

	;Run firefox (if doesn't already exist) with gathered url's, bring to focus and fullscreen
	SetTitleMatchMode, 2

	run firefox.exe %urlstring%

	if firefoxExists = false
		winwaitactive, Mozilla Firefox
	WinMaximize, Mozilla Firefox

	Return
}

getComicsOn() {
	global inputParam
	global weekDict
	global weekArray
	;parses single specified day and returns
	wordArray := StrSplit(inputParam, " ")


	;extract string, regMatch holds first char location

	dayNum := weekDict[wordArray[2]]
	day := weekArray[dayNum]

	return day
}


getComicsBetween() {
	global inputParam
	global weekDict
	global weekArray
	;parse inputParam and grab matching days
	;split string, take first and last as day1 and day2, loop through array (resetting to 1 if > 7), adding each day string to list separated by AND's
	;return list

	regMatch := RegExMatch(%inputParam%, "(sun|mon|tues|wednes|thurs|fri|satur|to|yester)day and (sun|mon|tues|wednes|thurs|fri|satur|to|yester)day")
	wordArray := StrSplit(%regMatch%, " ")

	;Wrong? doesn't skip since AHK arrays start at 1 - check!
	startNum := weekDict[wordArray[1]] ;skip the "and"
	endNum := weekDict[wordArray[3]]

	;psuedocode!!! NOT TESTED
	resultStrg := ""
	loop, (%endNum% - %startNum%)
	{
		if A_index > 1
			resultStrg += "AND " . weekArray[(%startNum% + %A_index% - 1)]
		else
			resultStrg += weekArray[(%startNum% + %A_index% - 1)]
	}

	return resultStrg
}

getComicsSoFar() {
	global todayNum
	global weekArray
	;calculate days between sunday and today
	;loop through array for days between sunday and today, adding each day string to list separated by AND's
	;return list

	;psuedocode!!! NOT TESTED
	resultStrg := ""
	loop, %todayNum%
	{
		if A_index > 1
			resultStrg += "AND " . weekArray[%A_index%]
		else
			resultStrg += weekArray[%A_index%] 
	}

	return resultStrg
}

getAllComics() {
	return "'Sunday' AND 'Monday' AND 'Tuesday' AND 'Wednesday' AND 'Thursday' AND 'Friday' AND 'Saturday'"
}