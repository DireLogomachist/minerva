#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn, LocalSameAsGlobal, Off  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force

;#Include %A_ScriptDir%\lib\AHK_DBA-IsNull\Lib\SQLite_L.ahk
;#Include %A_ScriptDir%\lib\AHK_DBA-IsNull\Lib\DBA.ahk

#Include <DBA>

placesDB := A_AppData . "\Mozilla\Firefox\Profiles\t2qenwmk.default\places.sqlite" 
;placesDB := A_AppData . "\Mozilla\Firefox\Profiles\e9rfzts7.default\places.sqlite" ;replace with string from config file

;Get day of the week
weekArray := ["'Sunday'", "'Monday'", "'Tuesday'", "'Wednesday'", "'Thursday'", "'Friday'", "'Saturday'"]
dayNum = %A_WDay%
dayString := weekArray[dayNum] ;arrays are 1 one based so no offset needed, Sunday starts at 1

;Gather urls from sqlite matching parent folder as day of week in show folder
;connect to sqlite file
databaseType := "SQLite"
connectionString = %placesDB%
urlstring := ""

;Get the parent day folder
try {
	currentDB := DBA.DataBaseFactory.OpenDataBase(databaseType, connectionString)
	try {
		getDayQuery := "SELECT id FROM moz_bookmarks WHERE parent IN (SELECT id FROM moz_bookmarks WHERE title IS 'Show Schedule' LIMIT 1) AND title IS " . daystring

		table := currentDB.Query(getDayQuery)

		if(IsObject(table) and table.Rows.Count() = 1) {
			dayId := table.Rows[1][1]
		} else if (table.Rows.Count() > 1) {
			TrayTip, Minerva, Multiple folders matching today found`nFolders must be unique, 10, 2
			sleep 10000
			return
		} else {
			TrayTip, Minerva, No shows bookmarked for today, 10, 1
			sleep 10000
			return
		}
	} catch e {
		TrayTip, Minerva, Unable to query bookmarks from sqlite, 10, 2
		sleep 10000
		return
	}
} catch e {
	TrayTip, Minerva, Unable to open Mozilla bookmarks sqlite, 10, 2
	sleep 10000
	return
}

if WinExist("ahk_class MozillaWindowClass") {
	firefoxExists = true
} else {
	firefoxExists = false
}

try {
	;odd behavior: with existing window current tab is last tab opened, without it is the first tab opened, so order is reversed depending for consistency
	getURLs := "SELECT url FROM moz_places JOIN moz_bookmarks ON moz_places.id = moz_bookmarks.fk WHERE parent IS " . dayId . " ORDER BY position"

	if firefoxExists = true
		getUrls := getUrls . " DESC"

	table := currentDB.Query(getURLs)
	if(IsObject(table)  and table.Rows.Count() > 0) {
		for each, row in table.Rows
		{
			if (%firefoxExists% = true)
			{
				urlstring := urlstring . " -new-tab " . row[1]
			} else
				urlstring := urlstring . " " . row[1]

		}
	} else {
		TrayTip, Minerva, No shows bookmarked for today, 10, 1
		sleep 10000
		return
	}
} catch e {
	TrayTip, Minerva, Unable to query bookmarks from sqlite, 10, 2
	sleep 10000
	return
}

;Run firefox (if doesn't already exist) with gathered url's, bring to focus and fullscreen
SetTitleMatchMode, 2

run firefox.exe %urlstring%

if firefoxExists = false
	winwaitactive, Mozilla Firefox
WinMaximize, Mozilla Firefox

Return