#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force

#Include lib/Name2Number.ahk

;Change behavior on command line argument
If 0 = 0
{
	;No cmd line args - increase by fixed amount
	Send {Volume_Up 1} ;for notifications, soundset alone doesn't raise standard notification
	Send {Volume_Down 1}
	SoundSet -5

	TrayTip, Minerva, Volume decreased by 5, 10, 1
	sleep 10000
} else {
	cmdInput =  %1%

	;Check if numerical, if not convert via online script Name2Number
	if cmdInput is digit
	{
		if cmdInput is not space
		{
			amount = %cmdInput%
		} else {
			amount := Name2Number(cmdInput)
		}
	} else {
		amount := Name2Number(cmdInput)
	}
	
	if amount is digit
	{
		if amount is not space 
		{
			soundAmount := "-" . amount
			Send {Volume_Up 1} ;for notifications, soundset alone doesn't raise standard notification
			Send {Volume_Down 1}
			SoundSet, %soundAmount%
			TrayTip, Minerva, Volume decreased by %amount%, 10, 1
			sleep 10000
		} else {
			TrayTip, Minerva, Unable to decrease volume with input`n'%amount%', 10, 2
			sleep 10000
		}
	} else {
		TrayTip, Minerva, Unable to decrease volume with input`n'%amount%', 10, 2
		sleep 10000
	}
}


Return