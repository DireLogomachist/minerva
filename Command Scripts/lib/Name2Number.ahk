; AutoHotkey: v1.1.11+
; Language:   English
; Platform:   Win7
; Author:     iPhilip
;
; Convert the name of a number into the number
;
; The function Name2Number(String) converts the name of a number into a number from 1 to 999,999,999,999.
; For example, the string "two hundred sixty-four thousand three hundred and fifty eight" converts to 264358.
; See http://en.wikipedia.org/wiki/List_of_numbers#Cardinal_numbers for a reference to the names of numbers.
; Numbers such as "eleven hundred" are not allowed. Use "one thousand one hundred" instead.
; Spaces and tabs are allowed at the start and end of the string and in between words.
; Hyphenated ("twenty-one") and non-hyphenated names ("twenty one") are allowed.
; The word "and" can be used inside the string but only after the words hundred, thousand, million, or billion.
;
; The function sets ErrorLevel to one of the following values:
;
;    0 - If the conversion was successful
;    1 - If there are typos ("elevn") or spaces inside hyphenated ("twenty- one") or regular ("seven teen") words
;    2 - If there are syntax errors in the string ("two hundred thirty ten" or "one-thirty")
;
; ------------------------------------------------------------------------------------------

Name2Number(Name)
{
   static Ones  := {one:1,two:2,three:3,four:4,five:5,six:6,seven:7,eight:8,nine:9}
   static Teens := {one:1,two:2,three:3,four:4,five:5,six:6,seven:7,eight:8,nine:9,ten:10,eleven:11,twelve:12,thirteen:13,fourteen:14,fifteen:15,sixteen:16,seventeen:17,eighteen:18,nineteen:19}
   static Tens  := {twenty:20,thirty:30,forty:40,fifty:50,sixty:60,seventy:70,eighty:80,ninety:90}
   static Thousands := {thousand:10**3,million:10**6,billion:10**9}
   static Dictionary := "and,one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen,seventeen,eighteen,nineteen,twenty,thirty,forty,fifty,sixty,seventy,eighty,ninety,hundred,thousand,million,billion"
;
; Strip extra spaces from the start and end of the string, as well as in between words
; Perform syntax checking and strip the hyphen from hyphenated words
; Spell check each word and assemble them into a temporary array
;
   String =                                               ; Initialize the temporary string
   ErrorLevel := 0                                        ; Initialize the error code
   Loop, Parse, Name, %A_Space%%A_Tab%                    ; Parse the name using spaces and tabs as delimeters
   {
      if A_LoopField =                                    ; Skip the blanks between multiple spaces
         Continue
      if (Hyphen := Instr(A_LoopField, "-"))              ; If there is a hyphen in the word ...
      {
         LeftSide := Substr(A_LoopField, 1, Hyphen - 1)   ; Extract the left side of the hyphenated word
         RightSide := Substr(A_LoopField, Hyphen + 1)     ; Extract the right side of the hyphenated word
         if not (Tens.HasKey(LeftSide) AND Ones.HasKey(RightSide))   ; If the left side is not in the Tens array and the right side is not in the Ones array ...
            ErrorLevel := 2                               ; Set the error code corresponding to a syntax error
         if LeftSide not in %Dictionary%                  ; If the left part of the word is not in the Dictionary ...
            ErrorLevel := 1                               ; Set the the error code corresponding to a typo or an illegal word
         if RightSide not in %Dictionary%                 ; If the right part of the word is not in the Dictionary ...
            ErrorLevel := 1                               ; Set the the error code corresponding to a typo or an illegal word
         if ErrorLevel                                    ; If the error code is non-zero ...
            Return                                        ; Return with a null result
         String .= LeftSide A_Space RightSide A_Space     ; Store the two parts of the hyphenated word in the temporary string
         Continue                                         ; Skip the rest of the loop
      }
      if A_LoopField not in %Dictionary%                  ; If the word is not in the Dictionary ...
      {
         ErrorLevel := 1                                  ; Set the the error code corresponding to a typo or an illegal word
         Return                                           ; Return with a null result
      }
      String .= A_LoopField A_Space                       ; Store the word in the temporary string
   }
   String = %String%                                      ; Remove the trailing space at the end of the string (assumes the default AutoTrim, On)
   StringSplit, Array, String, %A_Space%                  ; Put all the words into a temporary array
;
; Perform syntax checking on "and" words (see above) and remove them
; Put the remaining words in the string into an array
; Count the number of "word periods", i.e. the blocks of words that represent 3 digits (left to right)
; For example, the number "two million seventy-three thousand four hundred fifty one" (2,073,451)
; has 3 word periods: "two million", "seventy-three thousand", and "four hundred fifty-eight"
;
   NumPeriods := NumWords := 0                            ; Initialize the parameters
   Loop %Array0%                                          ; Loop through each word
      if (Array%A_Index% = "and")                         ; If the word is "and" ...
      {
         Prev := A_Index - 1                              ; Setup a variable for pointing to the previous word
         if (A_Index = 1 OR A_Index = Array0 OR not (Array%Prev% = "hundred" OR Thousands.HasKey(Array%Prev%)))
         {  ; If the "and" is at the start or at the end of the string or it doesn't immediately follow the words hundred, thousad, million, or billion ...
            ErrorLevel := 2                               ; Set the error code corresponding to a syntax error
            Return                                        ; Return with a null result
         }
      }
      else                                                ; If the word is not "and" ...
      {
         NumWords++                                       ; Increment the number of words in the string
         Word%NumWords% := Array%A_Index%                 ; Assign the current word to the word array
         if Thousands.HasKey(Array%A_Index%)              ; If the word is part of the Thousands array ...
            NumPeriods++                                  ; Increment the number of word blocks
      }
   if not Thousands.HasKey(Word%NumWords%)                ; If the last word is not part of the Thousands array, e.g. "two thousand *twelve*", ...
      NumPeriods++                                        ; Increment the number of blocks by one to include the ones block
;
; Convert the word array into a number
;
   Total := 0                             ; Initialize the running total for the result
   Index := 1                             ; Initialize the index that refers to the word that is being analyzed
   ErrorLevel := 0                        ; Initialize the error code
   Thou := Thousands.Clone()              ; Make a copy of the Thousands array as it gets "consumed" in the loop below
   Loop %NumPeriods%                      ; Iterate the loop below for each word block
   {
      Sum := 0                            ; Initialize the sum for each word block
      if Ones.HasKey(Word%Index%)         ; If first word is a number from 1-9, e.g. "*seven* hundred thousand four hundred", ...
      {
         Sum := Ones[Word%Index%]         ; Convert the word into a number
         if (++Index > NumWords)          ; If that's the last word in the string, e.g. "one thousand *four*", ...
            Return Total + Sum            ; Return the result
         if (Word%Index% = "hundred")     ; If the next word is "hundred", e.g. "seven *hundred* thousand", ...
         {
            Sum *= 100                    ; Multiply the number associated with the first word by 100
            if (++Index > NumWords)       ; If that's the last word in the string, e.g. "one thousand two *hundred*", ...
               Return Total + Sum         ; Return the result
         }
         else                             ; If the next word is not "hundred" ...
         {
            Sum := 0                      ; Reset the sum of the current word block
            Index--                       ; Reset the index to the start of the word block
         }                                ; The original condition "if Ones.HasKey(Word%Index%)" will be detected again with "if Teens.HasKey(Word%Index%)" below
      }                                   ; Note that without the above "else" block, the string "one two" would be allowed
      if Tens.HasKey(Word%Index%)         ; If the next word is a number in the Tens array, e.g. "two hundred *sixty* four million", ...
      {
         Sum += Tens[Word%Index%]         ; Add that number to the word block sum
         if (++Index > NumWords)          ; If that's the last word in the string, e.g. "two hundred *sixty*", ...
            Return Total + Sum            ; Return the result
         if Ones.HasKey(Word%Index%)      ; If the next word is a number from 1-9, e.g. "two hundred sixty *four* million", ...
         {
            Sum += Ones[Word%Index%]      ; Add that number to the word block sum
            if (++Index > NumWords)       ; If that's the last word in the string, e.g. "two hundred sixty *four*", ...
               Return Total + Sum         ; Return the result
         }
      }
      else if Teens.HasKey(Word%Index%)   ; If the next word is a number from 1-19, e.g. "two hundred *eighteen* million", ...
      {
         Sum += Teens[Word%Index%]        ; Add that number to the word block sum
         if (++Index > NumWords)          ; If that's the last word in the string, e.g. "two hundred *eighteen*", ...
            Return Total + Sum            ; Return the result
      }
      if Thou.HasKey(Word%Index%)         ; If the next word is a member of the Thousands array, e.g. "two hundred sixty four *million*", ...
      {
         ThouValue := Thou[Word%Index%]   ; Copy the value associated with the current word into a temporary variable
         Sum *= ThouValue                 ; Multiply the word block sum by that number
         if (Sum = 0)                     ; If the word block sum is zero ...
            Break                         ; Break out of the loop as it means that the string has a syntax error, e.g. "two thousand *thousand*"
         Array := Thou.Clone()            ; Copy the Thou array into a temporary array
         for Key, Value in Array          ; For each (Key,Value) pair in the temporary array ...
            if (Value >= ThouValue)       ; If the Value is larger than or equal than the value associated with the current word ...
               Thou.Remove(Key)           ; Remove the pair as a way to detect syntax errors, e.g. "two thousand one thousand"
         if (++Index > NumWords)          ; If that's the last word in the string, e.g. "two hundred sixty four *million*", ...
            Return Total + Sum            ; Return the result
      }
      else                                ; If the next word is not a member of the Thousands array ...
         Break                            ; Break out of the loop as it means that the string has a syntax error, e.g. "one one million"
      Total += Sum                        ; Add the word block sum to the running total
   }
   ErrorLevel := 2                        ; If the end of string is not detected because there is a syntax error, e.g. "four three", set the corresponding error code
   Return                                 ; Return with a null result
}